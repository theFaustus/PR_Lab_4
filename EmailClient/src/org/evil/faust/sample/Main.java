package org.evil.faust.sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.evil.faust.utils.EmailReceiver;
import org.evil.faust.utils.EmailSender;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../res/emailClient.fxml"));
        primaryStage.setTitle("Email Client v.1.0 Rights Reserved");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("org/evil/faust/res/envelope.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
