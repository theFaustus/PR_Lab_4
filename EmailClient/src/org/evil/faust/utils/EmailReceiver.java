package org.evil.faust.utils;

import java.io.*;
import javax.net.ssl.*;
import java.util.ArrayList;
import java.util.Map;


/**
 * Created by Faust on 3/25/2017.
 */
public class EmailReceiver implements Runnable {
    private ArrayList<Observer> observers = new ArrayList<>();

    private String server;
    private int port;
    private String username;
    private final String password;
    private SSLSocket socket;
    private int numberOfMessages;
    private boolean isWebContent = false;
    private BufferedReader input;
    private PrintWriter output;

    public EmailReceiver(String username, String password, String server, int port) {
        this.username = username;
        this.password = password;
        this.server = server;
        this.port = port;
        try {
            SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            socket = (SSLSocket) sslsocketfactory.createSocket(this.server, this.port);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            connect();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void connect() throws IOException, InterruptedException {
        String response = validateOneLine();
        //System.out.println(response);
    }

    private int getNumberOfMessages() {
        return numberOfMessages;
    }

    private String executeRETR(int indexOfMessage) throws IOException {
        if (indexOfMessage <= numberOfMessages) {
            String response = "";
            String result = "";
            output.println("RETR " + indexOfMessage);
            //System.out.println("RETR " + indexOfMessage);
            while (!socket.isClosed()) {
                response = validateMessageLine();
                result += response + "\n";
                System.out.println(response);
                if (response.equals("."))
                    break;
            }
            return result;
        } else
            return "err";
    }

    private String executeSTAT() throws IOException {
        String response;
        output.println("STAT");
        //System.out.println("STAT");
        response = validateOneLine();
        //System.out.println(response);
        String[] tokens = response.split(" ");
        numberOfMessages = Integer.parseInt(tokens[1]);
        return response;
    }

    private String executePASS(String password) throws IOException {
        String response;
        output.println("PASS " + password);
        //System.out.println("PASS " + password);
        response = validateOneLine();
        //System.out.println(response);
        return response;
    }

    private String executeUSER(String username) throws IOException {
        this.username = username;
        String response;
        output.println("USER recent:" + this.username);
        //System.out.println("USER recent:" + this.username);
        response = validateOneLine();
        //System.out.println(response);
        return response;
    }

    private void executeQUIT() throws IOException {
        output.println("QUIT");
        //System.out.println("QUIT");
        input.close();
        output.close();
        socket.close();
    }

    private String executeLIST() throws IOException {
        String response;
        output.println("LIST");
        //System.out.println("LIST");
        response = validateOneLine();
        //System.out.println(response);
        return response;
    }

    private String validateMessageLine() throws IOException {
        String response = input.readLine();
        if (response == null)
            return null;
        return response;

    }

    private String validateOneLine() throws IOException {
        String response = input.readLine();
        if (response == null)
            return null;

        if (response.startsWith("+OK"))
            return response;
        else {
            executeQUIT();
            return response;
        }
    }

    public void retrieveMails() {
        new Thread(this).start();
    }


    public void registerObserver(Observer o) {
        observers.add(o);
    }

    public void publishMail(Mail mail) {
        for (Observer o : observers)
            o.consumeMail(mail);
    }

    @Override
    public void run() {
        try {
            executeUSER(username);
            executePASS(password);
            executeSTAT();
            int numberOfMessages = getNumberOfMessages();

            for (int i = 1; i <= numberOfMessages; i++) {
                String mailBody = executeRETR(i);
                POP3RETRParser pop3RetrParser = new POP3RETRParser(mailBody);
                Mail mail = new Mail.MailBuilder(i)
                        .subject(pop3RetrParser.getHeaders().get("Subject"))
                        .to(pop3RetrParser.getHeaders().get("To"))
                        .from(pop3RetrParser.getHeaders().get("From"))
                        .date(pop3RetrParser.getHeaders().get("Date"))
                        .body(pop3RetrParser.getBody())
                        .isWebContent(pop3RetrParser.isWebContent())
                        .headers(pop3RetrParser.getHeaders())
                        .attachments(pop3RetrParser.getAttachments())
                        .build();
                publishMail(mail);
            }
            executeQUIT();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
