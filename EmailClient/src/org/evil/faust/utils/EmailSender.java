package org.evil.faust.utils;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.UnknownHostException;
import java.util.Base64;

/**
 * Created by Faust on 3/25/2017.
 */
public class EmailSender {
    private String password;
    private String server;
    private int port;
    private String username;
    private SSLSocket socket;

    private BufferedReader input;
    private PrintWriter output;

    public static String boundary = "DataSeparatorBoundary";

    public EmailSender(String username, String password, String server, int port) {
        this.server = server;
        this.port = port;
        this.username = username;
        this.password = password;
        try {
            SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            socket = (SSLSocket) sslsocketfactory.createSocket(this.server, this.port);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            if (input == null || output == null) {
                System.out.println("Failed to open streams to socket.");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeQUIT() {
        sendln("QUIT");
        output.close();

        try {
            input.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeDATA(String recipient, String subject, String body, String attachmentContent) throws IOException {

        sendln("DATA");
        sendln("MIME-Version: 1.0");
        sendln("Subject: " + subject);
        sendln("To: " + recipient);
        sendln("From: " + username);
        sendln("Content-Type: multipart/mixed; boundary=\"" + boundary + "\"");
        sendln("\r\n--" + boundary);
        sendln("Content-Type: text/plain; charset=\"UTF-8\"\r\n");
        sendln(body);
        sendln(attachmentContent);
        sendln("\r\n--" + boundary);
        sendln("\r\n\r\n--" + boundary + "--\r\n");
        sendln(".");
        String acceptedOK = input.readLine();
        System.out.println(acceptedOK);
    }

    private void sendln(String x) {
        System.out.println(x);
        output.println(x);
    }


    private void executeRCPT_TO(String recipient) throws IOException {
        sendln("RCPT TO:<" + recipient + ">");
        String recipientOK = input.readLine();
        System.out.println(recipientOK);
    }

    private void executeMAIL_FROM() throws IOException {
        sendln("MAIL From:<" + username + ">");
        String senderOK = input.readLine();
        System.out.println(senderOK);
    }

    private void executeAUTH_LOGIN() throws IOException {
        String usernameBase64 = new String(Base64.getEncoder().encode(username.getBytes()));
        String passBase64 = new String(Base64.getEncoder().encode(password.getBytes()));

        sendln("AUTH LOGIN");
        System.out.println(input.readLine());
        sendln(usernameBase64);
        System.out.println(input.readLine());
        sendln(passBase64);
        System.out.println(input.readLine());
    }

    private void executeHELO() throws IOException {
        String initialID = input.readLine();
        System.out.println(initialID);
        sendln("HELO " + server);
        String welcome = input.readLine();
        System.out.println(welcome);
    }

    public void send(String recipient, String subject, String body, String attachment) throws IOException {
        executeHELO();
        executeAUTH_LOGIN();
        executeMAIL_FROM();
        executeRCPT_TO(recipient);
        executeDATA(recipient, subject, body, attachment);
        executeQUIT();
    }


}
