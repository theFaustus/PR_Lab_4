package org.evil.faust.utils;

/**
 * Created by Faust on 3/26/2017.
 */
public interface Observer {
    void consumeMail(Mail mail);
}
