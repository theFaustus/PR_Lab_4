package org.evil.faust.utils;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeUtility;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Faust on 3/28/2017.
 */
public class POP3RETRParser {
    private String retrContent;
    private boolean isWebContent;
    private Map<String, String> headers = new HashMap<>();
    private Map<String, Attachment> mapOfAttachments = new HashMap<>();

    public POP3RETRParser(String retrContent) {
        this.retrContent = retrContent;
        this.headers.put("Delivered-To", executeHeaderRegex("(Delivered-To: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Received", executeHeaderRegex("(Received: )(by[\\s\\S]*?)(\\n)"));
        this.headers.put("X-Received", executeHeaderRegex("(X-Received: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Return-Path", executeHeaderRegex("(Return-Path: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Received", executeHeaderRegex("(Received: )(from[\\s\\S]*?)(\\n)"));
        this.headers.put("Received-SPF", executeHeaderRegex("(Received-SPF: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Authentication-Results", executeHeaderRegex("(Authentication-Results: )([\\s\\S]*?)(\\n)"));
        this.headers.put("DKIM-Signature", executeHeaderRegex("(DKIM-Signature: )([\\s\\S]*?)(==)"));
        this.headers.put("X-Google-DKIM-Signature", executeHeaderRegex("(X-Google-DKIM-Signature: )([\\s\\S]*?)(==)"));
        this.headers.put("X-Gm-Message-State", executeHeaderRegex("(X-Gm-Message-State: )([\\s\\S]*?)(\\n)"));
        this.headers.put("MIME-Version", executeHeaderRegex("(MIME-Version: )([\\s\\S]*?)(\\n)"));
        this.headers.put("References", executeHeaderRegex("(References: )([\\s\\S]*?)(\\n)"));
        this.headers.put("In-Reply-To", executeHeaderRegex("(In-Reply-To: )([\\s\\S]*?)(\\n)"));
        this.headers.put("From", executeHeaderRegex("(From: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Date", executeHeaderRegex("(Date: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Importance", executeHeaderRegex("(Importance: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Sensitivity", executeHeaderRegex("(Sensitivity: )([\\s\\S]*?)(\\n)"));
        this.headers.put("X-Priority", executeHeaderRegex("(X-Priority: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Message-ID", executeHeaderRegex("(Message-ID: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Subject", executeHeaderRegex("(Subject: )([\\s\\S]*?)(\\n)"));
        this.headers.put("To", executeHeaderRegex("(\\nTo: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Content-Type", executeHeaderRegex("(Content-Type: )([\\s\\S]*?)(\\n)"));
        this.headers.put("Content-Transfer-Encoding", executeHeaderRegex("(Content-Transfer-Encoding: )([\\s\\S]*?)(\\n)"));
    }

    public String getBody() {
        try {
            retrContent = MimeUtility.decodeText(retrContent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Pattern pattern = Pattern.compile("(Content-Type: text\\/html; charset=UTF-8\\nContent-Transfer-Encoding: quoted-printable)([\\s\\S]*?)(\\n\\n--)");
        Matcher matcher = pattern.matcher(retrContent);
        String body = "";
        while (matcher.find())
            body = matcher.group(2);
        //System.out.println(body);
        body = setIfIsWebContent(body);
        if (body != null) return body;

        pattern = Pattern.compile("(<!DOCTYPE html>)|(<html>)[\\s\\S]*?\\/html>");
        matcher = pattern.matcher(retrContent);
        body = "";
        while (matcher.find())
            body = matcher.group();
        //System.out.println(body);
        body = setIfIsWebContent(body);
        if (body != null) return body;

        pattern = Pattern.compile("(Content-Type: text\\/plain; charset=UTF-8|\"UTF-8\"|\"us-ascii\"|us-ascii)([\\s\\S]*?)(--)");
        matcher = pattern.matcher(retrContent);
        body = "";
        while (matcher.find())
            body = matcher.group(2);
        if (!body.trim().isEmpty()) return body;
        else
            return "This email has no content. :)";

    }

    public Map<String, Attachment> getAttachments() {
        try {
            retrContent = MimeUtility.decodeText(retrContent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Pattern pattern = Pattern.compile("(Content-Disposition: attachment;)( filename=)([\\s\\S]*?\\n)([\\s\\S]*?\\n)(\\n[\\s\\S]*?)(--)");
        Matcher matcher = pattern.matcher(retrContent);
        String filename = "";
        String attachmentEncoded = "";
        while (matcher.find()) {
            filename = matcher.group(3);
            attachmentEncoded = matcher.group(5);
            mapOfAttachments.put(filename, new Attachment(filename, attachmentEncoded));
        }

        //System.out.println(body);
        return mapOfAttachments;
    }

    private String setIfIsWebContent(String body) {
        if (!body.isEmpty()) {
            try {
                body = convertToUTF8(body);
            } catch (IOException | MessagingException e) {
                e.printStackTrace();
            }
            isWebContent = true;
            return body;
        } else {
            isWebContent = false;
        }
        return null;
    }

    private String convertToUTF8(String body) throws IOException, MessagingException {
        InputStream inputStream = IOUtils.toInputStream(body, "UTF-8");
        inputStream = MimeUtility.decode(inputStream, "quoted-printable");
        body = IOUtils.toString(inputStream, "UTF-8");
        return body;
    }

    private String executeHeaderRegex(String regex) {
        String result = "";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(retrContent);
        while (matcher.find()) {
            result = matcher.group(2);
            if (!result.isEmpty())
                break;
        }
        if (result.isEmpty())
            result = "<unknown>";
        return result;
    }

    public boolean isWebContent() {
        return isWebContent;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }
}
