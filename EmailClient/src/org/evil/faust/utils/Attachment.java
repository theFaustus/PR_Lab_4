package org.evil.faust.utils;

/**
 * Created by Faust on 3/30/2017.
 */
public class Attachment {
    private String nameOfFile;
    private String attachmentContent;

    public Attachment(String nameOfFile, String attachmentContent){
        this.nameOfFile = nameOfFile;
        this.attachmentContent = attachmentContent;
    }

    public String getNameOfFile() {
        return nameOfFile;
    }

    public String getAttachmentContent() {
        return attachmentContent;
    }
}
