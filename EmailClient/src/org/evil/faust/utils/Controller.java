package org.evil.faust.utils;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import org.apache.commons.io.FilenameUtils;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.net.URL;
import java.util.*;

import static org.evil.faust.utils.EmailSender.boundary;

public class Controller implements Observer, Initializable {
    public Pane mainPaneId;
    public Pane welcomePaneId;
    public Pane fillPaneId;
    public Pane viewMailPane;
    public Pane composeEmailPane;

    public TextField usernameText;
    public TextField passwordText;
    public TextField smtpServerNameText;
    public TextField smtpPortText;
    public TextField pop3ServerNameText;
    public TextField pop3PortText;
    public TextField usernameTextField;
    public TextField passwordTextField;
    public TextField smtpServerNameTextField;
    public TextField smtpPortTextField;
    public TextField pop3ServerNameTextField;
    public TextField pop3PortTextField;
    public TextField viewMailDateTextField;
    public TextField viewMailSubjectTextField;
    public TextField viewMailToTextField;
    public TextField viewMailFromTextField;
    public TextField composeMailSubjectTextField;
    public TextField composeMailToTextField;

    public TextArea composeMailBodyTextField;
    public TextArea viewMailBodyTextField;

    public TableColumn<Mail, String> mailSubjectColumn;
    public TableColumn<Mail, String> dateReceivedColumn;
    public TableColumn<Mail, String> senderColumn;
    public TableColumn<Attachment, String> sentAttachmentsColumn;
    public TableColumn<Attachment, String> receivedAttachmentsColumn;
    public TableView mailTableView;
    public TableView sentAttachmentsTableView;
    public TableView receivedAttachmentsTableView;


    public CheckBox inboxCheckbox;
    public CheckBox sentCheckbox;
    public CheckBox allCheckbox;
    public StackPane webViewPane;
    public WebView mailBodyWebView;
    public TextArea allHeadersTextArea;


    private Set<Mail> mails = new HashSet<>();
    private File attachmentFile;
    private String attachContent = "";
    private ArrayList<Attachment> listOfAttachments;
    private String lastInAttachment = "";


    @FXML
    public void onSetupButtonPressed(ActionEvent actionEvent) {
        welcomePaneId.setVisible(false);
    }

    @FXML
    public void onGoButtonPressed(ActionEvent actionEvent) {
        usernameText.setText(usernameTextField.getText());
        passwordText.setText(passwordTextField.getText());
        smtpServerNameText.setText(smtpServerNameTextField.getText());
        smtpPortText.setText(smtpPortTextField.getText());
        pop3PortText.setText(pop3PortTextField.getText());
        pop3ServerNameText.setText(pop3ServerNameTextField.getText());
        fillPaneId.setVisible(false);
    }

    @FXML
    public void onRefreshButtonPressed(ActionEvent actionEvent) {
        mailTableView.getItems().clear();
        EmailReceiver emailReceiver = new EmailReceiver(usernameText.getText(), passwordText.getText(), pop3ServerNameText.getText(), Integer.parseInt(pop3PortText.getText()));
        emailReceiver.registerObserver(this);
        emailReceiver.retrieveMails();
    }

    @FXML
    public void onCloseViewMailButton(MouseEvent mouseEvent) {
        viewMailPane.setVisible(false);
    }

    @FXML
    public void onCloseComposeMailButton(MouseEvent mouseEvent) {
        composeEmailPane.setVisible(false);
    }

    @FXML
    public void onComposeButtonPressed(ActionEvent actionEvent) {
        listOfAttachments = new ArrayList<>();
        sentAttachmentsTableView.getItems().clear();
        composeMailBodyTextField.clear();
        composeEmailPane.setVisible(true);
    }

    @FXML
    public void onAttachButtonPressed(ActionEvent actionEvent) {
        attachmentFile = getOpenAttachmentFile();
        if (attachmentFile != null) {
            MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
            if (((double) attachmentFile.length()) / (1024 * 1024) > 1) {
                alertThatFileSizeExceeds();
                return;
            }
            String mimeType = mimeTypesMap.getContentType(attachmentFile);
            attachContent +=
                    "\r\n--" + boundary + "\r\n" +
                            "Content-Type: " + mimeType + "; name=" + attachmentFile.getName() + "\n" +
                            "Content-Disposition: attachment; filename=\"" + attachmentFile.getName() + "\"\n" +
                            "Content-Transfer-Encoding: base64\r\n\r\n" + MIMEBase64.encode(attachmentFile);
            Attachment attachment = new Attachment(attachmentFile.getName(), attachContent);
            lastInAttachment = attachment.getNameOfFile();
            if (!listOfAttachments.contains(attachment)) {
                listOfAttachments.add(attachment);
                sentAttachmentsTableView.getItems().add(attachment);
                sentAttachmentsTableView.scrollTo(attachment);
            }
            //System.out.println(attachment);
        } else {
            alertThatFileNotLoaded();
            return;
        }

    }

    private void alertThatFileSizeExceeds() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like you cannot get a file bigger than 1Mb.\nTry to chose another file.", ButtonType.CLOSE);
        alert.setHeaderText("Size Exceeds");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }

    private File getOpenAttachmentFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose attachment");
        fileChooser.getExtensionFilters()
                .addAll(new FileChooser.ExtensionFilter("Files", "*.*")); // files
        return fileChooser.showOpenDialog(null);
    }

    private File getSaveAttachmentFile(String fileName) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose location");
        fileChooser.setInitialFileName(new String(fileName));
        fileChooser.getExtensionFilters()
                .addAll(new FileChooser.ExtensionFilter("Files", "*.*")); // files
        return fileChooser.showSaveDialog(null);
    }

    @FXML
    public void onSendButtonPressed(ActionEvent actionEvent) {
        EmailSender emailSender = new EmailSender(usernameText.getText(), passwordText.getText(), smtpServerNameText.getText(), Integer.parseInt(smtpPortText.getText()));
        try {
            if (composeMailSubjectTextField.equals("") || composeMailBodyTextField.equals("") || composeMailToTextField.equals("")) {
                alertThatSomethingIsMissing();
            } else {
                if (!composeMailToTextField.getText().matches("^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$")) {
                    String allAttachments = "";
                    for (Attachment a : listOfAttachments)
                        allAttachments += a.getAttachmentContent();
                    emailSender.send(composeMailToTextField.getText(), composeMailSubjectTextField.getText(), composeMailBodyTextField.getText(), allAttachments);
                    composeEmailPane.setVisible(false);
                } else
                    alertThatEmailIsBroken();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onDeleteButtonPressed(ActionEvent actionEvent) {
        if (!listOfAttachments.isEmpty()) {
            Attachment attachment = listOfAttachments.get(listOfAttachments.size() - 1);
            listOfAttachments.remove(listOfAttachments.size() - 1);
            sentAttachmentsTableView.getItems().remove(attachment);
        }
    }

    @FXML
    public void alertThatSomethingIsMissing() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like you forgot about something.\nTry to fill all the fields.", ButtonType.CLOSE);
        alert.setHeaderText("Something is missing");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }

    @FXML
    public void alertThatEmailIsBroken() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like you entered a wrong email.\nEmail should look like mike@gmail.com.", ButtonType.CLOSE);
        alert.setHeaderText("Broken email format");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }

    @FXML
    public void alertThatFileNotSaved() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like something went wrong.\nWe couldn`t save your file.", ButtonType.CLOSE);
        alert.setHeaderText("Not saved");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }

    @FXML
    public void alertThatFileNotLoaded() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like something went wrong.\nWe couldn`t load your file.", ButtonType.CLOSE);
        alert.setHeaderText("Not loaded");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }


    @Override
    public void consumeMail(Mail mail) {
        mails.add(mail);
        Platform.runLater(() -> {
            if (allCheckbox.isSelected())
                mailTableView.getItems().add(mail);
            if (sentCheckbox.isSelected())
                if (mail.getFrom().equals(usernameText.getText()))
                    mailTableView.getItems().add(mail);
            if (inboxCheckbox.isSelected())
                if (!mail.getFrom().equals(usernameText.getText()))
                    mailTableView.getItems().add(mail);
            mailTableView.scrollTo(mail);
        });

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mailTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                viewMailDateTextField.setText(((Mail) obs.getValue()).getDate());
                viewMailFromTextField.setText(((Mail) obs.getValue()).getFrom());
                viewMailToTextField.setText(((Mail) obs.getValue()).getTo());
                viewMailSubjectTextField.setText(((Mail) obs.getValue()).getSubject());
                receivedAttachmentsTableView.getItems().clear();
                for (Map.Entry<String, Attachment> e : ((Mail) obs.getValue()).getMapOfAttachments().entrySet()) {
                    receivedAttachmentsTableView.getItems().add(e.getValue());
                    receivedAttachmentsTableView.scrollTo(e.getValue());
                }

                String headers = "";
                for (Map.Entry<String, String> e : ((Mail) obs.getValue()).getHeaders().entrySet())
                    headers += e.getKey() + " = " + e.getValue() + "\n";
                allHeadersTextArea.setText(headers);

                if (!((Mail) obs.getValue()).isWebContent()) {
                    webViewPane.setVisible(false);
                    viewMailBodyTextField.setText(((Mail) obs.getValue()).getBody());
                } else {
                    webViewPane.setVisible(true);
                    WebEngine webEngine = mailBodyWebView.getEngine();
                    webEngine.loadContent(((Mail) obs.getValue()).getBody());
                }
                viewMailPane.setVisible(true);

            }
        });


        receivedAttachmentsTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                String attachmentContent = ((Attachment) obs.getValue()).getAttachmentContent();
                String nameOfFile = ((Attachment) obs.getValue()).getNameOfFile();
                nameOfFile = nameOfFile.replace("\"", "");
                attachmentContent = attachmentContent.replace("\n", "").replace("\r", "");
                byte[] data = Base64.getDecoder().decode(attachmentContent);
                File saveAttachmentFile = getSaveAttachmentFile(nameOfFile);
                if (saveAttachmentFile != null)
                    try (OutputStream stream = new FileOutputStream(saveAttachmentFile)) {
                        stream.write(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                else {
                    alertThatFileNotSaved();
                    return;
                }
            }
        });

        mailSubjectColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getSubject()));
        dateReceivedColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getDate()));
        senderColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getFrom()));
        sentAttachmentsColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNameOfFile()));
        receivedAttachmentsColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNameOfFile()));


        allCheckbox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                sentCheckbox.setSelected(false);
                inboxCheckbox.setSelected(false);
            }

        });

        inboxCheckbox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                allCheckbox.setSelected(false);
                sentCheckbox.setSelected(false);
            }

        });

        sentCheckbox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                inboxCheckbox.setSelected(false);
                allCheckbox.setSelected(false);
            }

        });
    }


}
