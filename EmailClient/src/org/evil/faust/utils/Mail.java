package org.evil.faust.utils;

import java.util.Map;

/**
 * Created by Faust on 3/26/2017.
 */
public class Mail {
    private int indexOfMail;
    private String subject;
    private String from;
    private String to;
    private String body;
    private String date;
    private boolean isWebContent;
    private Map<String, String> headers;
    private Map<String, Attachment> mapOfAttachments;


    public Mail(MailBuilder mailBuilder) {
        this.indexOfMail = mailBuilder.indexOfMail;
        this.subject = mailBuilder.subject;
        this.from = mailBuilder.from;
        this.to = mailBuilder.to;
        this.body = mailBuilder.body;
        this.date = mailBuilder.date;
        this.isWebContent = mailBuilder.isWebContent;
        this.headers = mailBuilder.headers;
        this.mapOfAttachments = mailBuilder.mapOfAttachments;
    }

    public boolean isWebContent() {
        return isWebContent;
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getBody() {
        return body;
    }

    public String getDate() {
        return date;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, Attachment> getMapOfAttachments() {
        return mapOfAttachments;
    }

    @Override
    public String toString() {
        return String.format("Mail(%d) :%n%s%n%s%n%s%n%s%n%s", indexOfMail, subject, date, to, from, body);
    }

    public static class MailBuilder {
        private int indexOfMail;
        private String subject;
        private String from;
        private String to;
        private String body;
        private String date;
        private boolean isWebContent;
        private Map<String, String> headers;
        private Map<String, Attachment> mapOfAttachments;

        public MailBuilder(int indexOfMail) {
            this.indexOfMail = indexOfMail;
        }

        public MailBuilder to(String to) {
            this.to = to;
            return this;
        }

        public MailBuilder attachments(Map<String, Attachment> mapOfAttachments) {
            this.mapOfAttachments = mapOfAttachments;
            return this;
        }

        public MailBuilder headers(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        public MailBuilder from(String from) {
            this.from = from;
            return this;
        }

        public MailBuilder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public MailBuilder body(String body) {
            this.body = body;
            return this;
        }

        public MailBuilder date(String date) {
            this.date = date;
            return this;
        }

        public MailBuilder isWebContent(boolean isWebContent) {
            this.isWebContent = isWebContent;
            return this;
        }

        public Mail build() {
            Mail mail = new Mail(this);
            return mail;
        }

    }
}
